

class DagConfig(object):

    def __init__(self, cfg_file):
        self.cfg_file = cfg_file

    def read_config(self):
        import json
        with open(self.cfg_file, 'r') as cfg_data:
            json.load()

