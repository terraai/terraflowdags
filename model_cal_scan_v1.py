import airflow
from airflow.models import DAG
from airflow.operators.subdag_operator import SubDagOperator
from airflow.operators.python_operator import PythonOperator

from sub_dags.ensemble_simulation import ensemble_flow_simulation

from base_model import Run, Realization

import os
import json
from datetime import datetime

#
#       Naming section
#

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

def run_dag(config):

    run = Run(config)

    args = {
        'owner': 'airflow',
        'start_date': datetime(2018, 4, 30)
    }

    main_dag = DAG(
        dag_id=dag_name,
        default_args=args,
        schedule_interval='@once'
    )

    max_iterations = run.max_iterations
    # max_realizations = run.max_realizations

    def download(config):
        run = Run(config)
        if os.path.exists(run.directory):
            print('Input already exists')
        else:
            run.download()

    def validate_no_existing_run(config):
        from copy import deepcopy
        cfg2 = deepcopy(config)
        cfg2['run']['input']['type'] = 'run'
        cfg2['run']['input']['name'] = cfg2['run']['run_id']
        run = Run(cfg2)
        if os.path.exists(run.directory):
            raise ValueError

    def calibrate(config, i):
        from sys import platform
        import os
        run = Run(config)
        max_real = run.max_realizations
        real = Realization(config, r=1, i=i + 1)

        # Make the base folder
        if platform.lower()[:3] == 'win':
            os.system('mkdir -p ' + '\\'.join(real.directory.split('\\')[:-1]))
            # Common model files dir
            iter_dir = '\\'.join(real.directory.split('\\')[:-1]) + '\\' + real.realization_prefix
        else:
            os.system('mkdir -p ' + '/'.join(real.directory.split('/')[:-1]))
            # Common model files dir
            iter_dir = '/'.join(real.directory.split('/')[:-1]) + '/' + real.realization_prefix

        print('Copying base')
        print('From dir = ' + real.base_model_dir)
        print('To dir = ' + iter_dir)
        os.system(' '.join(['cp -R ', real.base_model_dir, iter_dir]))

        for r in range(1, max_real + 1):
            old_real = Realization(config, r=r, i=i)
            print('old realization = ' + old_real.directory)
            real = Realization(config, r=r, i=i+1)
            print('new realization = ' + real.directory)
            os.system(' '.join(['cp -R ', old_real.directory, real.directory]))

    validate = PythonOperator(
        task_id='validate_run_output_does_not_exist_' + str(dag_iter),
        python_callable=validate_no_existing_run,
        op_args=[config],
        default_args=args,
        dag=main_dag
    )

    download = PythonOperator(
        task_id='download_source_files_to_efs_' + str(dag_iter),
        python_callable=download,
        op_args=[config],
        default_args=args,
        dag=main_dag
    )

    simulation_iterations = []
    calibration_iterations = []

    for i in range(1, max_iterations + 1):
        subdag_name = 'run-{}-simulation-iteration-{}'.format(str(run_id), str(i))
        simulation_iterations.append(SubDagOperator(
            task_id=subdag_name,
            subdag=ensemble_flow_simulation(dag_name, subdag_name, args, config, i),
            default_args=args,
            dag=main_dag
        ))

        update_task_id = 'run-{}-update-iteration-{}'.format(str(dag_iter), str(i))
        calibration_iterations.append(PythonOperator(
            task_id=update_task_id,
            python_callable=calibrate,
            op_args=[config, i],
            default_args=args,
            dag=main_dag
        ))

    main_dag >> validate >> download >> simulation_iterations[0]

    for i in range(0, max_iterations):

        simulation_iterations[i] >> calibration_iterations[i]
        if i != max_iterations - 1:
            calibration_iterations[i] >> simulation_iterations[i+1]

    return main_dag
#
#               RUN CONFIG
#

config_dir = 'configs'
configs = os.listdir(config_dir)

dags = []

# Loop through configs and put dags in global namespace
for config_file in configs:

    if config_file.split('.')[1].lower() == 'json':
        print('Loading {}'.format(config_file))
        config_path = os.path.join(config_dir, config_file)
        with open(config_path, 'r') as config_data:
            config = json.load(config_data)

        run_id = config['run']['run_id']
        dag_name = 'project_{}_run_{}_config_{}'.format(config['project']['name'],
                                                        config['run']['run_id'],
                                                        config_file)
        globals()[dag_name] = run_dag(config)
        # dags.append(run_dag(config))
