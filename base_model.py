class Project(object):
    """
    Parent directory for the project
    """

    def __init__(self, cfg):
        self.cfg = cfg
        # self.level = self.__class__.__name__.lower()
        self.level = 'project'

        self.name = cfg[self.level]['name']
        self.directory = cfg[self.level]['local_data_directory']
        self.backend_name = cfg[self.level]['backend']['name']

        if self.backend_name == 's3':
            self.bucket = cfg[self.level]['backend']['bucket']
            self.s3_url = '/'.join(['s3:/', self.bucket])
            self.s3_url = self._get_s3_url()
            self.s3_prefix = self.name + '/'

        self.directory = self._get_directory()

        self.project_name = self.name
        self.project_directory = self.directory

    def _get_s3_prefix(self):
        # Not used in base project class
        return self.s3_prefix + self.name + '/'

    def _get_s3_url(self, extra_folders=None, back_dir=0):
        s3_url = self.s3_url
        # print('step 1 = ' + s3_url)
        if back_dir != 0:
            s3_url = '/'.join(s3_url.split('/')[:-back_dir])
            # print('step 2 = ' + s3_url)
        if extra_folders:
            s3_url = '/'.join([s3_url, str(extra_folders)])
            # print('step 3 = ' + s3_url)

        s3_url = '/'.join([s3_url, self.name])
        return s3_url

    def _get_directory(self, extra_folders=None, back_dir=0):
        from sys import platform

        directory = self.directory.replace('\\', '/')
        # print('step 1 ' + directory)

        if back_dir != 0:
            directory = '/'.join(directory.split('/')[:-back_dir])
            # print('step 2 ' + directory)
        if extra_folders:
            directory = '/'.join([directory, str(extra_folders)])
            # print('step 3 ' + directory)

        directory = '/'.join([directory, self.name])
        # print('step 4 ' + directory)

        if platform.lower()[:3] == 'win':
            directory = directory.replace('/', '\\')

        return directory

    def __getitem__(self, item):
        return self.__dict__.get(item, None)

    def make_directory(self):
        import os
        import errno
        print('Making ' + '/'.join([self.directory]))
        try:
            os.makedirs(self.directory, exist_ok=True)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(self.directory):
                pass
            else:
                raise

    def list_folders(self, backend=None):
        model_list = []
        if backend == 's3':
            import boto3

            s3 = boto3.resource('s3')
            bucket = s3.Bucket(name=self.bucket)
            # Important prefix ends in '/' such as 'Olympus/Well_Placement/ECLIPSE/'
            # _prefix = 'Olympus/Well_Placement/ECLIPSE/'
            _prefix = self.s3_prefix
            result = bucket.meta.client.list_objects_v2(Bucket=bucket.name,
                                                        Prefix=_prefix,
                                                        Delimiter='/')
            for o in result.get('CommonPrefixes'):
                model_list.append(o.get('Prefix').split('/')[-2])
        else:
            import os
            model_list = os.listdir(self.directory)
            for m in model_list:
                if model_list[:1]:
                    model_list.remove(m)

        return model_list

    def download(self):
        from os import system
        self.make_directory()
        if self.backend_name == 's3':
            import os
            system(' '.join(['aws s3 cp', self.s3_url, self.directory, '--recursive']))

    def upload(self):
        from os import system
        if self.backend_name == 's3':
            import os
            system(' '.join(['aws s3 cp', self.directory, self.s3_url, '--recursive']))

    def write_config(self):
        import json
        cfg_name = '/'.join([self.directory, '.'.join(['.' + self.name + '_config', 'json'])])
        print('Writing config to %s' % self.directory)
        with open(cfg_name, 'w') as output:
            json.dump(self.cfg[self.level], output)

    def update_config(self):
        pass


class Model(Project):
    """
    Class for models within the project
    """

    def __init__(self, cfg):
        super(Model, self).__init__(cfg)
        self.cfg = cfg
        # self.level = self.__class__.__name__.lower()
        self.level = 'model'
        self.name = cfg[self.level]['name']
        self.realization_prefix = cfg[self.level]['realization_prefix']

        self.directory = self._get_directory()
        self.s3_url = self._get_s3_url()
        self.s3_prefix = self._get_s3_prefix()

        self.model_directory = self.directory
        self.model_name = self.name

        # Performance
        # self.model_inputs = self.list_inputs()

    def list_inputs(self):
        import os
        folder_list = os.listdir(self.directory)
        if 'Runs' in folder_list:
            folder_list.remove('Runs')
        return folder_list


class Input(Model):
    def __init__(self, cfg):
        super(Input, self).__init__(cfg)
        self.level = 'model'
        self.name = self.cfg['model']['input_name']

        self.directory = self._get_directory()
        self.s3_url = self._get_s3_url()

        self.input_directory = self.directory


class Realization(Input):

    def __init__(self, cfg, r, i):
        super(Realization, self).__init__(cfg)
        self.cfg = cfg
        self.r = r
        self.i = i

        self.iteration_directory_name = 'iteration_' + str(i)
        self.next_iteration_directory_name = 'iteration_' + str(i + 1)

        # We should be able to support working with realizations without having to do a run. This qualifies that we
        # even have a run attribute in the config
        if 'run' in self.cfg.keys():
            self.run_current_iteration = Run(self.cfg).current_iteration
            self.run_current_realization = Run(self.cfg).current_realization
            self.run_input_name = Run(self.cfg).input_name
            self.run_id = Run(self.cfg).run_id

        if 'simulator' in self.cfg.keys():
            self.simulator = self.cfg['simulator']['name']
            self.simulator_settings = self.cfg['simulator']['settings']
            self.mpi_settings = self.cfg['simulator']['mpi_settings']
            if 'license' in self.cfg['simulator'].keys():
                if 'license_server_ip' in self.cfg['simulator']['license'].keys():
                    # TODO implement a license class for this
                    self.license_server_ip = self.cfg['simulator']['server_ip']
                # else:
                #     print('No license server specified')

        if self.i == 1:
            # Use the input from the model level
            print('i = 1')
            self.name = '_'.join([self.realization_prefix, str(r)])
            self.directory = self._get_directory()
            self.s3_url = self._get_s3_url()
            self.data_file_name = self.name
        elif self.i <= self.run_current_iteration:
            # If we pick up off of a iteration off of a prior run
            print('Current iteration')
            self.name = '_'.join([self.realization_prefix, str(r), str(i)])
            extra_folders = '/'.join(['Runs', self.run_input_name, self.iteration_directory_name])
            self.directory = self._get_directory(extra_folders=extra_folders, back_dir=1)
            self.s3_url = self._get_s3_url(extra_folders=extra_folders, back_dir=2)
            # TODO There has to be a better way than splitting this
            self.data_file_name = '_'.join(self.name.split('_')[:-1])
        else:
            # This is for new runs
            # print('Next iteration')
            self.name = '_'.join([self.realization_prefix, str(r), str(i)])
            extra_folders = '/'.join(['Runs', self.run_id, self.iteration_directory_name])
            self.directory = self._get_directory(extra_folders=extra_folders, back_dir=1)
            self.s3_url = self._get_s3_url(extra_folders=extra_folders, back_dir=2)
            # TODO Same
            self.data_file_name = '_'.join(self.name.split('_')[:-1])

        # self.base_model_dir = self._get_base_model_dir()
        self.destination_directory = self._get_destination_dir()

    @property
    def base_model_dir(self):
        from sys import platform
        if platform.lower()[:3] == 'win':
            output = '\\'.join([self.input_directory, self.data_file_name.split('_')[:1][0]])
        else:
            output = '/'.join([self.input_directory, self.data_file_name.split('_')[:1][0]])
        return output

    # def _get_base_model_dir(self):
    #     self.__tmp_name = self.name
    #     self.name = self.name.split('_')[:1][0]
    #     extra_folders = '/'.join([self.input_directory, self.iteration_directory_name])
    #     directory = self._get_directory(extra_folders=extra_folders, back_dir=2)
    #     self.name = self.__tmp_name
    #     return directory
    #
    # def _get_base_model_s3_url(self):
    #     self.__tmp_name = self.name
    #     self.name = self.name.split('_')[:1][0]
    #     extra_folders = '/'.join(['Runs', self.run_id, self.iteration_directory_name])
    #     s3_url = self._get_s3_url(extra_folders=extra_folders, back_dir=2)
    #     self.name = self.__tmp_name
    #     return s3_url


    def _get_destination_dir(self):
        """
        Gets the next iteration directory
        :return: destination_directory
        """
        # Temporarily init the name for _get_directory method
        self.name = '_'.join([self.realization_prefix, str(self.r), str(self.i + 1)])
        extra_folders = '/'.join([self.run_id, self.next_iteration_directory_name])
        destination_directory = self._get_directory(extra_folders=extra_folders, back_dir=3)
        # Reinit name
        if self.i == 1:
            self.name = '_'.join([self.realization_prefix, str(self.r)])
        elif self.i <= self.run_current_iteration:
            self.name = '_'.join([self.realization_prefix, str(self.r), str(self.i)])
        else:
            self.name = '_'.join([self.realization_prefix, str(self.r), str(self.i)])
        return destination_directory

    # TODO Idea to automatically get the prefix for the realization
    def _get_prefix(self):
        pass

    def _get_sim_file_full_path(self):
        from sys import platform
        if platform.lower()[:3] == 'win':
            input_path = '\\'.join([self.directory, self.data_file_name + '.DATA'])
        else:
            input_path = '/'.join([self.directory, self.data_file_name + '.DATA'])
        return input_path

    def run_simualtion(self):
        input_path = self._get_sim_file_full_path()
        print(input_path)
        FlowSimulation(input_path=input_path,
                       simulator=self.simulator,
                       sim_settings=self.simulator_settings,
                       mpi_settings=self.mpi_settings).run()


class Run(Model):
    def __init__(self, cfg):
        super(Run, self).__init__(cfg)
        self.level = 'run'
        self.run_id = cfg[self.level]['run_id']

        self.schedule_interval = cfg[self.level]['schedule_interval']
        self.start_date = cfg[self.level]['start_date']

        self.max_iterations = cfg[self.level]['max_iterations']
        self.current_iteration = cfg[self.level]['current_iteration']
        self.current_realization = cfg[self.level]['current_realization']
        self.max_realizations = cfg[self.level]['max_realizations']

        self.run_type = cfg[self.level]['run_type']

        if cfg['run']['input']['type'] != 'run':
            self.name = cfg['model']['input_name']
            self.directory = self._get_directory()
            self.s3_url = self._get_s3_url()
            self.s3_prefix = self._get_s3_prefix()
        else:
            self.name = cfg[self.level]['input']['name']
            extra_folders = '/'.join(['Runs'])
            self.directory = self._get_directory(extra_folders=extra_folders)
            self.s3_url = self._get_s3_url(extra_folders=extra_folders)
            self.s3_prefix = self._get_s3_prefix()

        self.input_name = self.name

        # Reinitialize name to actual output dir
        # extra_folders = '/'.join(['Runs', self.run_id])
        # self.directory = self._get_directory(extra_folders=extra_folders, back_dir=1)

    def _make_new_run_dir(self):
        import os
        if os.path.exists(self.directory):
            counter = 0
            while os.path.exists(self.directory):
                counter += 1
                self.directory = ''.join([self.directory, '_v_', str(counter)])
                print(str(counter))
        print(self.directory)
        self.make_directory()

    def start_new(self):
        self._make_new_run_dir()
        if self.current_iteration == 1:
            pass
        raise NotImplementedError

    def run_ensemble(self):
        for i in range(1, self.max_iterations + 1):
            flow_sim = FlowSimulation()
            flow_sim.run()
        raise NotImplementedError


class Grid(Realization):
    def parse_grdecl(self, file_name):
        pass


class Porosity(Grid):
    pass


class Permeability(Grid):
    pass


class FlowSimulation(object):
    """
    Single forward run simulation
    """

    def __init__(self,
                 simulator,
                 sim_settings,
                 input=None,
                 sim_deck_name=None,
                 input_path=None,
                 simulator_directory=None,
                 mpi_settings=None,
                 license_server_ip=None,
                 license_server_port=None,
                 license_path=None
                 ):

        self.simulator = simulator
        self.sim_settings = sim_settings
        # For now we're just going to use this. When other sims come up we can be more flexible with how we
        # handle simulation extensions and match them with a dictionary as below
        self.input = input
        # These are not implemented. Expecting full path
        self.sim_deck_name = sim_deck_name
        self.input_path = input_path
        # This shouldn't be needed as we have the simulator in the PATH env var
        self.simulator_directory = simulator_directory
        self.mpi_settings = mpi_settings
        self.license_server_ip = license_server_ip
        self.license_server_port = license_server_port
        self.license_path = license_path

        if self.simulator_directory:
            self.simulator = '/'.join([self.simulator_directory, self.simulator])

    def _flatten_sim_settings(self):
        __settings_string = ""
        for k, v in self.sim_settings.items():
            __setting = '='.join([str(k), str(v)])
            __settings_string = ' '.join([__settings_string, __setting])
        return __settings_string

    def _flatten_mpi_settings(self):

        __mpi_settings_string = ''
        for k, v in self.mpi_settings.items():
            __mpi_setting = ' '.join(['-' + str(k), str(v)])
            __mpi_settings_string = ' '.join([__mpi_settings_string, __mpi_setting])
        return __mpi_settings_string

    def _validate_license_server_conn(self):
        """
        Run nc or lmgrd based on what kind of simulator it is and validate that the connection is valid
        :return:
        """
        from os import system
        validation_cmd = ' '.join(['nc -vz', self.license_server_ip, self.license_server_port])
        system(validation_cmd)

    def run(self):
        from os import system

        simulators = {
            'opm_flow': {
                'cmd': 'flow',
                'data_file_extension': '.DATA'
            },
            'mrst': {
                'cmd': 'mrst',
                'data_file_extension': '.DATA'
            },
            'eclipse': {
                'cmd': 'eclipse',
                'data_file_extension': '.DATA'
            },
            'echelon': {
                'cmd': 'echelon',
                'data_file_extension': '.DATA'
            }}
        if self.simulator not in simulators.keys():
            error_msg = "{} not in list of acceptable simulators {}".format(self.simulator, str(simulators.keys()))
            raise ValueError(error_msg)

        sim_cmd = simulators[self.simulator]['cmd']

        if self.mpi_settings:
            mpi_settings_string = self._flatten_mpi_settings()
            sim_cmd = ' '.join(['mpirun', mpi_settings_string, sim_cmd])

        if self.sim_settings:
            settings_string = self._flatten_sim_settings()
            sim_cmd = ' '.join([sim_cmd, settings_string])

        # Unless we need to have special commands per simulator, this is the execution lines
        execution_cmd = ' '.join([sim_cmd, self.input_path])
        system('echo execution_cmd')
        system('echo ' + execution_cmd)
        print(execution_cmd)
        system(execution_cmd)


if __name__ == '__main__':
    import json
    import os
    with open('json_configs/testing-win.json', 'r') as data:
        cfg = json.load(data)

    # pj = Project(cfg)
    #
    # md = Model(cfg)
    #
    # run = Run(cfg)
    # run.download()
    # print(run.directory)
    #
    real = Realization(cfg, i=4, r =2)
    real.run_simualtion()

    # def download(config):
    #     run = Run(config)
    #     run.download()
    #
    # download(config=cfg)
    #
    # def calibrate(config, i):
    #     import shutil
    #     run = Run(config)
    #     max_real = run.max_realizations
    #     for r in range(1, max_real + 1):
    #         old_real = Realization(config, r=r, i=i)
    #         real = Realization(config, r=r, i=i + 1)
    #         shutil.copytree(old_real.directory, real.directory)
    #
    # calibrate(config=cfg, i=1)

    # inp = Input(cfg)
    # print(inp.directory)
    #
    # rz = Realization(cfg, r=1, i=2)
    # # rz.run_simualtion()
    # print(rz.name)
    # print(rz.directory)
    # print(rz.input_directory)
    # print(rz.data_file_name)
    # print(rz.base_model_dir)
    # print(rz.directory.split('\\')[:-1])

    # import shutil
    # from sys import platform
    #
    # real = Realization(cfg, r=50, i=1+1)
    # dirs = '\\'.join(real.directory.split('\\')[:-1])
    # print(dirs)
    # print(real.realization_prefix)
    # if platform.lower()[:3] == 'win':
    #     iter_dir = '\\'.join(real.directory.split('\\')[:-1]) + '\\' + real.realization_prefix
    # else:
    #     iter_dir = '/'.join(real.directory.split('/')[:-1])+ '/' + real.realization_prefix
    # print('Copying base')
    # print('From dir = ' + real.base_model_dir)
    # print('To dir = ' + iter_dir)
    # shutil.copytree(real.base_model_dir, iter_dir)

    # def calibrate(config, i):
    #     import shutil
    #     from sys import platform
    #     run = Run(config)
    #     max_real = run.max_realizations
    #     for r in range(1, max_real + 1):
    #         old_real = Realization(config, r=r, i=i)
    #         print('old realization = ' + old_real.directory)
    #         real = Realization(config, r=r, i=i + 1)
    #         print('new realization = ' + real.directory)
    #         shutil.copytree(old_real.directory, real.directory)
    #     if platform.lower()[:3] == 'win':
    #         iter_dir = '\\'.join(real.directory.split('\\')[:-1])
    #     else:
    #         iter_dir = '/'.join(real.directory.split('/')[:-1])
    #     print('Copying base')
    #     shutil.copytree(real.base_model_dir, iter_dir)
    #
    # calibrate(cfg, 1)

    # from copy import deepcopy
    # cfg2 = deepcopy(cfg)
    # cfg2['run']['input']['type'] = 'run'
    # cfg2['run']['input']['name'] = cfg2['run']['run_id']
    # old_run = Run(cfg2)
    # print(old_run.directory)
    # print(cfg2['run']['input']['type'])
    # real_run = Run(cfg)
    # print(real_run.directory)
    # print(cfg['run']['input']['type'])
    #
    # if os.path.exists(real_run.directory):
    #     print('here')

    # rz.upload()
