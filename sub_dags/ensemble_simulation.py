from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

from base_model import Run, Realization

from datetime import timedelta

def pre_sim_setup():
    pass


def run_sim(config, r, i):
    print('Running sim r = {} and iteration = {}'.format(str(r), str(i)))
    real = Realization(config, r=r, i=i)
    print('Input simulation folder = {}'.format(real.directory))
    real.run_simualtion()


def post_sim_check():
    pass


def ensemble_flow_simulation(parent_dag_name, child_dag_name, args, config, i):
    run = Run(config)
    max_realizations = run.max_realizations

    ensemble_subdag = DAG(
        dag_id='%s.%s' % (parent_dag_name, child_dag_name),
        default_args=args,
        schedule_interval='@once'
    )

    pre = PythonOperator(
        task_id='presim-setup',
        python_callable=pre_sim_setup,
        # op_args=[r],
        default_args=args,
        dag=ensemble_subdag
    )

    post = PythonOperator(
        task_id='post-sim-check',
        python_callable=post_sim_check,
        # op_args=[r],
        default_args=args,
        dag=ensemble_subdag
    )

    sims = []
    for r in range(1, max_realizations + 1):
        sims.append(PythonOperator(
            task_id='Run_{}_Iteration_{}_Realization_{}'.format(run.run_id, str(i), str(r)),
            python_callable=run_sim,
            op_args=[config, r, i],
            default_args=args,
            dag=ensemble_subdag,
            retries=10,
            retry_delay=timedelta(minutes=1),
            on_retry_callback=run_sim,
            queue='cpusims'
        ))

        pre >> sims[r - 1] >> post

    return ensemble_subdag
