import json
from base_model import Run, Realization

config_file = 'testing-s3fs-v2.json'
with open(config_file, 'r') as config_data:
    config = json.load(config_data)


def calibrate(config, i):
    from sys import platform
    import os
    run = Run(config)
    max_real = run.max_realizations
    real = Realization(config, r=1, i=i + 1)

    # Make the base folder
    if platform.lower()[:3] == 'win':
        print('mkdir -p ' + '\\'.join(real.directory.split('\\')[:-1]))
        # Common model files dir
        iter_dir = '\\'.join(real.directory.split('\\')[:-1]) + '\\' + real.realization_prefix
    else:
        print('mkdir -p ' + '/'.join(real.directory.split('/')[:-1]))
        # Common model files dir
        iter_dir = '/'.join(real.directory.split('/')[:-1]) + '/' + real.realization_prefix

    print('Copying base')
    print('From dir = ' + real.base_model_dir)
    print('To dir = ' + iter_dir)
    print(' '.join(['cp -R ', real.base_model_dir, iter_dir]))

    for r in range(1, max_real + 1):
        old_real = Realization(config, r=r, i=i)
        print('old realization = ' + old_real.directory)
        real = Realization(config, r=r, i=i + 1)
        print('new realization = ' + real.directory)
        print(' '.join(['cp -R ', old_real.directory, real.directory]))


calibrate(config=config, i=2)
