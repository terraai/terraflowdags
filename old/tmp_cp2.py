import json
from base_model import Run, Realization
from multiprocessing import Pool
import os

config_file = 'testing-s3fs-v2.json'
with open(config_file, 'r') as config_data:
    config = json.load(config_data)

def calibrate(config, i):
    # import shutil
    from sys import platform
    import os
    run = Run(config)
    max_real = run.max_realizations
    for r in range(1, max_real + 1):
        old_real = Realization(config, r=r, i=i)
        print('old realization = ' + old_real.directory)
        real = Realization(config, r=r, i=i+1)
        print('new realization = ' + real.directory)
        # os.system('ls ' + old_real.directory)
        print('mkdir -p ' + '/'.join(real.directory.split('/')[:-1]))
        print(' '.join(['cp -R ', old_real.directory, real.directory]))
        # shutil.copytree(old_real.directory, real.directory)
    # For common files
    if platform.lower()[:3] == 'win':
        iter_dir = '\\'.join(real.directory.split('\\')[:-1]) + '\\' + real.realization_prefix
    else:
        iter_dir = '/'.join(real.directory.split('/')[:-1])+ '/' + real.realization_prefix
    print('Copying base')
    print('From dir = ' + real.base_model_dir)
    print('To dir = ' + iter_dir)
    print(' '.join(['cp -R ', real.base_model_dir, iter_dir]))
    # shutil.copytree(real.base_model_dir, iter_dir)

calibrate(config=config, i=2)